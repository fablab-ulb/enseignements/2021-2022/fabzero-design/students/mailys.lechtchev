# Projet final

## Compte-rendu : étapes de la construction d’un objet de design

Classifier le design ? **Outil** VS **Signe**
**Outil** -> c’est l’usage. L’outil répond à un besoin ou une fonction particulière. VS **Signe** -> c’est l’apparence. Le signe répond à l’esthétique de l’objet.

1.	**Déconstruction de la fonction de l’objet**
Il est nécessaire d’observer et de regarder l’objet, se placer comme un anthropologue ou encore comme un ornithologue. Être rigoureux et méticuleux quand on se place en tant qu’**observateur**.
Un bon objet est un objet qui a appris des autres.
Afin de donner une image, c’est la caricature d’une personne qui en regarde une autre dans une cuisine pour voir les fonctionnements.

2.	**Résoudre un vrai problème**
Il faut comprendre les **contraintes** de l’objet, penser à l’équilibre de chaque élément. Comment réfléchir en fonctions des **besoins** ?
Développer une auto-critique. Vais-je dans le bon sens ?

3.	**Comprendre les dimensions spécifiques**
En tant que designer, on se doit de **ressentir** l’objet. Parfois, on ne le voit plus car on est assis dessus par exemple, comment le sentir ?
Penser au poids de l’objet ; est-ce agréable au toucher, à soulever, à prendre en main ? Il y a automatiquement des **choix techniques** à prendre en compte.
Il peut être intelligent d’utiliser une *toise* et d’observer les *positions* du corps face à l’objet. L’exemple typique avec le Modulor de le Corbusier où la position fait la forme.

4.	**Esquisse, essai-erreurs, améliorations**
Il faut impérativement passer par l’étape de **vérification d’un fonctionnement** dit « sale » *(tests réalisés à la main)*. A contrario des prototypes propres *(maquettes)* ou fonctionnels *(versions de l’objet qui fonctionnent)*.
Cela peut aider à définir les différentes techniques et machines à utiliser. Comprendre la souplesse du matériau, revenir sur des étapes précédentes, etc.

5.	**Adaptation, diffusion, édition**
Il est important de comprendre qu’un objet de design doit être réfléchi, conçu pour peut-être faire un jour partie d’une **collection** ou d’une **série**. C’est pour cela qu’il est nécessaire de documenter l’objet (comme nous le faisons avec Gitlab) avec toutes les informations utiles à la **reproduction** et la **diffusabilité**.
Il faut comprendre que l’objet créé est au point de départ **unique** mais qu’il peut être décliné pour sa diffusion.

6.	**Un nom, un pictogramme**
Il est indispensable de créer un **logo**, quelque chose où l’on va repérer l’objet parmi tant d’autres, il faut qu’il se détache.
L’idée ici est de ne pas s’arrêter à un **signe**, un simple mot ou phrase publicitaire, mais d’en créer carrément un fil rouge. Le logo, par exemple, peut s’apparenter à un pliage comme pour le vélo Brompton.



## Compte-rendu : chemin critique à suivre

Fonction – décomposition -> étapes, défauts

-	Travailler en **spirale**, revenir aux étapes du début quand il y a besoin (indispensable pour arriver à un objet final)
-	**Rapport à la personne** primordial
-	Partir d’**éléments connus** pour trouver le bon truc (dimensionnement, poids, présentation, production, technique)
-	Choix de la matière, temporalité, défauts du matériau, texture, éthique (compromis, paresse), adaptabilité

L’objet : le chemin
1.	Usiné : recherche d’un designer -> projet -> **interaction** et **interprétation** des deux partis
2.	Usiné (plus pour les meubles par exemple) : designer en premier lieu -> demande d’achat de projet -> **amélioration** en prenant l’avis de l’usine

Exemples de designs :
-	Sifflet de Cuba : partir avec peu d’outils, matière sur place, inventivité -> **déconstruction** de l’objet, en l’occurrence une canette -> **réduire** l’objet au minimum, l’objet en fonction de l’**humain** (taille, poigne, etc.)
-	Pantalon de vélo : trop lourd et imposant, comment réfléchir ? Zone à protéger ? -> facilité de pose, discrétion, matière réfléchissante, légèreté, facile à ranger

Pour clôturer, le design est différent d’un projet d’architecture. Le designer, lui, va créer des **prototypes**, les tester. Il en devient un **artisan**. L’architecte, quant à lui, va effectivement créer des **représentations** mais la grande différence ici est que le projet imaginé **n’existera peut-être jamais**.


## Compte-rendu musée : entrevues avec les professeurs

Voilà les quelques entrevues avec les différents professeurs qui nous ont aidé à faire avancer le projet.

**Première entrevue** avec les professeurs et les modules 3 qui travaillent au musée, choix des projets et des groupes de travail

-	Partir de l’usage et du signe, l’importance de l’objet.
-	Comparer, contraster les points communs et les différences, voir les relations entre les choses du quotidien de l’enfant
-	Redessin par les enfants
-	Déconstruire l’objet -> comprendre
-	S’asseoir avec eux, être à la même hauteur qu’eux, instaurer un calme
-	Privilégier la mémoire visuelle
-	Faire entrer les enfants dans quelque chose de déjà prémâché -> poser la question de créer un projet est impossible
-	Avoir des règles connues
-	Activités à faire au bon moment (parabole de dissipation)

Idées des modules 3 : cartes de jeux, cartes d’observation, réalité virtuelle, moules de fabrication
Autres ? Devinettes quizz, matière à toucher

**Deuxième entrevue** avec Victor et Yan, le professeur des enfants

-	Usage : sécurité, comment gérer les enfants ?
-	Formes trop simplistes, ce ne sont plus des enfants en bas-âge
-	Combien de moules pour 30 minutes d’activité ?
-	Scénario, déroulement de la visite : le guide doit savoir les bases du travail
-	Matière ? Terre glaise, plâtre, papier mâché
-	Chercher les différences entre les 18 objets choisit et voir les techniques
-	Plan B : affiche avec colonnes, dessiner l’objet trouvé avec la bonne technique

**Troisième entrevue** avec les professeurs de design

-	Fondre la bougie à 45° ok pour les enfants *(biberon = 37° (température du corps humain))*
-	Moule dur pour bougie à chercher
-	Extrusion avec cire ? Pas trop liquide
-	Diviser plus le moule -> plus de facilité
-	Designer le moule
-	Solution du savon ?
-	Kit : penser à tout, casserole etc.
-	Préparation : rapport au plastique, différent de ce que l’on fait
-	Objet : rapport enfant/prof, enfant/objet, enfant/parents

**Quatrième entrevue** : pré-jury

-	Manière « usine », comment le présenter ? Package, trouver un nom
-	Moules : arrondir les angles, épaisseur – pour + de rapidité de séchage, les numéroter et ranger dans des bacs
-	Boite pour ramener
-	Décider d’un élément commun (couleur, objet, forme)
-	Revoir le scénario : texte trop brut
-	Mode d’emploi et règles, comment se comporter avec les machines


## Introduction à l’exercice final

Après avoir eu nos formations de Git, Fusion 360, impression 3D, découpe laser, vinyle et découpeuse CNC, les quelques personnes du module 1 qui souhaitaient travailler avec le musée et les enfants rejoignent les modules 3. J’en fais évidemment partie.

Nous avons eu l’occasion de lire les comptes-rendus de nos camarades du module 3 où ils expliquent notamment les différentes techniques de fabrication du plastique, ce qui nous a grandement aidé pour la suite des événements. Voici les différents sites : [Eliott](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-design/students/eliott.steylemans/), [Louise](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-design/students/louise.vandeneynde/), [Maxime](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/), [Tejhay](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-design/students/tejhay.pinheiroalbia/). Avec Amina, nous avons rejoint Maxime, qui travaille sur les moules de fabrication. Sur son site, il détaille les tests qu’il a pu réaliser avant notre arrivée.

Durant la semaine projet, certains étudiants on eut l’occasion de faire une visite du musée avec les 24 enfants avec qui nous allons travailler. Je n’ai malheureusement pas pu y aller car j’étais en voyage d’étude. Je vous invite à aller voir les comptes-rendus de [Amina](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-design/students/amina.belguendouz/FabZero-Modules/musee-compte-rendu/), [Louise](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-design/students/louise.vandeneynde/FabZero-Modules/module05/) et [Maxime](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-design/students/maxime.wauthij/FabZero-Modules/module05/).


## Explorations, prototypes, matières

**Semaine 1**

-	Recherches sur les différentes techniques de fabrication ; injection, extrusion, soufflage, rotomoulage, thermoformage. Vidéos et tutos pour bien comprendre tous les fonctionnements
-	Recherches des matières possibles : pâte culinaire, polymère, pâte fimo, colle chaude, silicone, cire
-	Recherches de matières qui faciliteraient le décollement du moule : farine, huile, crème, poudre
-	Recherches de techniques : congeler le moule, cheminée, boucher les trous, attaches
-	Recherches sur les différents motifs possibles

| **Injection** | **Tests** |
| ------ | ------ |
| Impression d’un moule en étoile réalisé par Maxime. Test avec un pistolet à colle chaude. | ![](docs/images/injection.jpg) |
| Nous étions occupés avec le rotomoulage à ce moment-là et n’avons donc plus pensé à notre colle qui séchait dans le moule à injection. Malheureusement, nous avons attendu bien trop longtemps et il a été très compliqué d’ouvre le moule. Nous avons cassé les parties externes qui ne servaient pas à grand-chose finalement. Nous avons également essayé avec des moyens de pression. Le moule a complètement été détruit et l’étoile n’est pas satisfaisante. | ![](docs/images/etoile.jpg) |

| **Extrusion** | **Tests** |
| ------ | ------ |
| Dessin et impression de nouveaux moules à extrusion. Suite à l’entretien avec les profs, il fallait des formes plus simples pour privilégier la technique à la forme. | ![](docs/images/extrusion1.jpg) |

| **Rotomoulage** | **Tests** |
| ------ | ------ |
| Premier essai d’une machine à rotomoulage, redessin par Amina. La découpe à la Shaper a été compliquée car l’épaisseur du bois trouvé au Fablab est conséquente. Nous avons dû passer 2 fois avec une profondeur de 6mm et ce n’était pas encore suffisant. Nous laissons donc tomber la Shaper pour privilégier une scie sauteuse. Le travail est possible comme cela. | ![](docs/images/découperoto.jpg) |
| Les finitions ne sont pas terribles mais notre but était surtout de vérifier la faisabilité de cette technique. | ![](docs/images/morceaux.jpg) |
| Suite à cela, il faut modeler un moule pour aller avec la machine. Amina décide de modéliser une pokéball. | ![](docs/images/pokéball.jpg) |

**Semaine 2**

-	Recherches de nouvelles formes à sélectionner pour l’extrusion, l’injection, le soufflage et le rotomoulage
-	Recherche sur la technique du soufflage : pâte à ballon ? Ballon à souffler dans une bouteille pour obtenir la forme ?
-	Abandon de la technique du thermoformage (source de chaleur trop dangereuse pour les enfants)
-	Amélioration du moule pokéball : ajout de trous, d’attaches
-	Amélioration du moule d’injection : ajout d’attaches

| **Injection** | **Tests** |
| ------ | ------ |
| Impression d’un moule d’essai, cette fois-ci avec une forme plus simple. | ![](docs/images/mouleinj.jpg) |
| Injection de la colle chaude dans le moule. | ![](docs/images/injection1.jpg) |
| C’était pratiquement ce que nous espérions ! Nous l’avons sorti au bout de 21 minutes. Pour mieux faire, il me semble qu’au bout de 18 minutes ça aurait été très bien. Nous n’avons pas réessayé car nous avons trouvé une meilleure matière à utiliser pour nos moules. | ![](docs/images/pastille.jpg) |

| **Extrusion** | **Tests** |
| ------ | ------ |
| Premier test d’un moule avec de la pâte fimo. Test satisfaisant mais pas encore parfait. Fonctionne tout de même mieux que les tests précédents de Maxime (pâte à sucre). | ![](docs/images/fimo.jpg) |
| Le deuxième test consiste en l’impression d’un moule qui permettrait le changement de la forme. Il est muni d’entailles dans le boitier qui accueillerait des plaques en plastique découpées au laser. | ![](docs/images/boitier.jpg) |

| **Rotomoulage** | **Tests** |
| ------ | ------ |
| Montage de la machine à rotomouler. | ![](docs/images/roto.jpg) |
| Tests du moule avec du plâtre. Dosages différents pour obtenir des liquidités différentes. | ![](docs/images/recipient.jpg) |
| Le premier test n’est pas génial. Pas d’uniformité, certains endroits sont très épais, le temps de séchage est incommensurable. | ![](docs/images/platre.jpg) |
| Le deuxième test se fait avec un autre dosage de l’eau et surtout, avec d’un côté rien et de l’autre côté de l’huile de coco. Cette dernière fut un échec, l’huile de coco ne fait qu’empirer. Le plâtre n’a aucune tenue, il coule. Concernant le mélange, il est mieux réalisé, plus uniforme et l’esthétique est là. Malheureusement, le temps de séchage est de nouveau beaucoup trop long. | ![](docs/images/platre1.jpg) |
| Troisième test avec de la cire. Grande quantité d’huile de coco badigeonnée et quelques minutes d’attente. Le résultat est satisfaisant, cette technique peut fonctionner. Nous décidons de la conserver pour l’injection et l’extrusion également. | ![](docs/images/cire.jpg) |

![](docs/images/videoroto.mp4)

**Semaine 3**

-	Abandon de la technique de soufflage, concentration sur 3 techniques sûres
-	Recherches sur la cire : technique de chauffe au bain-marie ?
-	Recherches sur le savon, peut être une solution ?
-	Dessin des moules sur Fusion et Inkscape
-	Recherches sur les bonnes températures à adapter pour la cire chaude
-	Recherches sur le temps d’attente pour un bon séchage de la cire

| **Extrusion** | **Tests** |
| ------ | ------ |
| Dessin et impression de nouveaux moules qui s’adaptent plus facilement à la prise en main. | ![](docs/images/extrusion2.jpg) |
| Dessin et découpe des plaques qui s’emboitent dans le moule. | ![](docs/images/plaques.jpg) |
| Test avec la cire mi-dure mi-liquide. | ![](docs/images/cirepate.jpg) |
| Impression 3D d’une forme autour de laquelle on vient déposer la pâte sortie du moule d’extrusion. | ![](docs/images/dondolo.jpg) |

| **Rotomoulage** | **Tests** |
| ------ | ------ |
| Redessin et découpe de la machine avec un arrondissement des angles, des finitions plus propres. | ![](docs/images/roto1.jpg) |
| Nouveau moule créé par Amina. Système d’attaches comme pour l’injection et séparation en plus de parties qu’avant afin de faciliter le démoulage. | ![](docs/images/roto2.jpg) |

| **Venues des enfants** | **Crash test** |
| ------ | ------ |
| Observation des choses qui fonctionnent ou non.
Les enfants sont très intéressés et intègrent rapidement les informations données. Ils comprennent vite les modes de production et demandent immédiatement de pouvoir toucher. L’aspect manuel du projet est un plus.
Concernant ces techniques, le rotomoulage est la technique qui a le plus été appréciée et la cire est un bon moyen de l‘exploiter.
Pour l’extrusion, le malaxage de la cire chaude fonctionne plutôt bien mais le temps pour obtenir une forme est très court. Au contact de l’air et du froid, la pâte durcit relativement vite. Nous ne pouvons pas garder la cire pour cette technique de fabrication.
Malheureusement dans chacun des cas, nous n’avons pas réussi à avoir d’objet fabriqué concret mais les enfants ont au moins pu s’initier aux techniques de fabrication. | ![](docs/images/enfants.jpg) |

**Semaine 4**

-	Recherches sur le système de manivelle pour le rotomoulage
-	Scénario : écriture : décomposition des étapes
-	Suite des dessins des modes d’emploi pour les machines par Amina
-	Recherches sur le moyen d’intéresser les enfants durant la visite du musée
-	Recherches sur les objets à sélectionner
-	Plastification des documents produits

| **Injection** | **Tests** |
| ------ | ------ |
| Impression d’un nouveau moule d’injection par Amina. Cela fonctionne mais l’épaisseur intérieure mériterait plus d’épaisseur pour la stabilité de la forme sortie. | ![](docs/images/injection2.jpg) |

| **Extrusion** | **Tests** |
| ------ | ------ |
| Impression d’un nouveau moule d’extrusion. Forme plus allongée afin de sortir plus de matière. | ![](docs/images/extrusion3.jpg) |
| Fin rectangle comme forme afin de sortir une languette qui viendra prendre la forme d’un objet imprimé en 3D. | ![](docs/images/languette.jpg) |
| La pâte fimo part ensuite au micro-onde. | ![](docs/images/micro-onde.jpg) |
| Autre test, avec un bras de levier. Dessin et découpe du moule à la Shaper. | ![](docs/images/levier.jpg) |

| **Rotomoulage** | **Tests** |
| ------ | ------ |
| Prise des mesures et compréhension du système, dessin et impression par Maxime des pièces pour le système de manivelle. | ![](docs/images/engrenages.jpg) |
| Découpe de la nouvelle base pour la machine. Petit souci avec la découpe. Le trou n’était pas assez grand, il a fallu corriger cela. | ![](docs/images/base.jpg) |
| Dessin et découpe d’un volant par Amina pour tourner la machine avec le système mis en place. | ![](docs/images/volant.jpg) |

**Modes d’emploi**

![](docs/images/emploi.jpg)

**Jeu dans le musée**

Sous forme d’objets à retrouver dans le musée et à classer selon les techniques apprises par les enfants.

![](docs/images/Jeu.jpg)

**Scénario**

Imaginé pour les enfants et professeurs qui viendront participer aux activités et visiter le musée

![](docs/images/scénario1.jpg) ![](docs/images/Scénario2.jpg)

**Semaine finale : Derniers ajustements**

-	Recherches sur des boites à fabriquer (pour emporter ce que chaque enfant a fait)
-	Entrevue avec Cristina concernant les modalités
-	Modification des modes d’emploi et des règles
-	Recherches sur un nouveau système d’attache pour le rotomoulage
-	Choix des couleurs, créer un ensemble

| **Injection** | **Objets finaux** |
| ------ | ------ |
| Impression des moules finaux. Corrections des arrondis et du jeu d’emboitement. | ![](docs/images/injection3.jpg) |

| **Extrusion** | **Objet final** |
| ------ | ------ |
| Réalisation d’un nouveau bras de levier en métal. Ajout d’une poignée et modification du moule, une partie est maintenant filetée. Quant aux formes qui permettent la sortie de la matière, elles se glissent dans le moule. | ![](docs/images/extrusion4.jpg) |

| **Rotomoulage** | **Objets finaux** |
| ------ | ------ |
| Ajustement du système de rotation et impression d’une poignée. Changement de fixation du moule, création d’un filet avec du tissu. | ![](docs/images/rotomoulage.jpg) |

**Boite à outils**

Pour finaliser toutes ces recherches, voici la Fabrico’ Plastik, l’atelier qui englobe les techniques de fabrication. Nous avons créé une boite à outils (dessin, découpe, montage et décoration) pour englober les objets réalisés.

![](docs/images/fabrico.jpg)

**Boite puzzle**

Création d’une boite pour reprendre chez soi les objets créés. Les résultats ne sont pas satisfaisants. Après plusieurs essais et par manque de temps, nous ne sommes pas arrivés à quelque chose de fonctionnel.

## Modes d’emploi

[Liens vers les fichiers](https://drive.google.com/drive/folders/125OcMZjaauAgGs51joy6qQvgC2qeRrz0?usp=sharing)

## Règles et accompagnements

[Lien vers les fichiers](https://drive.google.com/drive/folders/125OcMZjaauAgGs51joy6qQvgC2qeRrz0?usp=sharing)
