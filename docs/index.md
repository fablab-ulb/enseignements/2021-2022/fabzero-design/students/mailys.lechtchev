Hello, moi c’est Maïlys, j’ai 22 ans et je suis actuellement en Master 1 d’architecture à la Cambre-Horta à Ixelles. J’étais auparavant à la faculté de l’UCL LOCI à Saint-Gilles et j’ai décidé de migrer vers d’autres horizons l’année passée. Pour cette année, nous avons eu à choisir une question d’architecture, mon choix s’est donc porté sur le cours de design car je voulais explorer de nouvelles choses.

Chers lecteurs, vous pourrez donc suivre mon apprentissage sur ce site, et, qui sait, peut-être apprendre à votre tour !

## En savoir plus sur moi

![](images/moi.jpg)

Qui suis-je ? Tout d’abord je suis née dans le village de Wellin, situé dans les Ardennes belges. J’ai passé toutes mes années de primaire là-bas. Pour les secondaires, je suis partie les deux dernières années en technique arts plastiques, ce qui me vaut une passion pour le dessin, la peinture, etc. Ces deux années étaient à mes yeux les plus belles et m’ont confirmé mon choix d’études. Après ma première année à LOCI, je suis partie quelques mois en Asie pour découvrir ce qu’était un voyage en sac à dos. Une superbe expérience qui ne m’a pas du tout porté préjudice quand je suis revenue pour la Bac 2. Me voilà maintenant arrivée en master 1 pour la suite de mon cursus universitaire.

## Mes intérêts

J’ai bien des passions dans la vie mais difficile de combiner cela avec des études aussi complètes que celles que j’ai entamée. La crise du covid n’a non plus pas arrangé les choses. Par exemple, l’année passée en septembre, je m’étais lancée dans des séances de parkour au Solbosch. Malheureusement, tout s’est arrêté au bout de trois semaines dû aux confinements perpétuels. Dans le même domaine, j’aime beaucoup l’escalade aussi. Je suis d’ailleurs partie il y a quelques jours explorer la grotte du père Noël, c’était super chouette mais très physique aussi (j’ai encore mal aux cuisses). Cette grotte fait partie des Grottes de Han mais elle n’est pas praticable par les visiteurs car il n’y a pas de chemins prévus comme pour les visites normales.

![](images/grottes.jpg)

Comme dit plus haut, j’ai également un amour pour l’art mais quand on ne pratique plus, cela se perd vite. J’aimerais beaucoup m’y remettre sérieusement et les études d’architecture peuvent clairement aider à cela.

![](images/expo-mai-2016.jpg)

Et ma dernière passion, la meilleure, est la musique. Je chante beaucoup mais je ne retiens pas facilement les paroles, c’est un peu un comble. Finalement, je peux dire que je m’intéresse facilement aux choses, le problème est que je peux me lasser ou bien tout simplement ne pas aller au bout des choses.


## Les enfants et le Bruxelles Design Museum

Si je me mets à la place d’un enfant quand je vois une œuvre de design, je pense que je serais intriguée ; par la forme, par la couleur ; comment l’objet fonctionne-t-il ? Notre mission sera de les initier à cet art, j’aspire à les rendre intéressés. Cependant, ce n’est pas chose simple car il faudra faire coïncider avec justesse l’enfant touche-à-tout et le musée.

L’intérêt pour moi de le faire dans le cadre de ce cours, est d’avoir la possibilité de travailler en équipe, chercher des solutions sur un domaine encore peu connu pour moi et de le mixer avec des acteurs principaux ou non.
