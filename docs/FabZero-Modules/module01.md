# 1. Apprentissage de GIT

## A quoi sert GitLab?

Globalement, GitLab est une plateforme de développement collaborative qui permet d'héberger et de gérer des projets web. Tout en permettant la gestion de tout le processus de développement, elle permet également la gestion de versions des codes sources. Cela est vraiment pratique pour garder chaque trace du projet.

L'outil que je trouve intéressant sur cette plateforme est la gestion d'*issues*. C'est une tâche à effectuer qui existe sous forme de problème de code à résoudre ou bien sous forme de suggestion d'amélioration. Tous les membres du projets peuvent participer à cela.

## Configuration du logiciel

Pour travailler sur cette plateforme depuis chez soi, un certain nombre d'étapes est à effectuer pour y parvenir. La première étape était de vérifier si le logiciel GitBash était déjà présent ou non dans l'ordinateur. J'ai donc vérifié sur l'invité de commandes de Windows.

![](../images/terminal-git.png) ![](../images/git-bash.png)

Ensuite, il faut configurer git. **git config --global user.name "mailys.lechtchev"
git config --global user.email "mailys.lechtchev@ulb.be"**

Après cela, il faut taper la commande **git config --global --list** afin de vérifier si les informations sont correctes.

Une fois ces étapes d'identifications faites, il faut se laisser dans la création de clés SSH. Pour cela, il faut taper la commande **ssh-keygen -t ed25519 -C "<mailys.lechtchev@ulb.be>"**.
J'obtiens comme réponse : *Generating public/private ed25519 key pair.
Enter file in which to save the key (/c/Users/lecht/.ssh/id_ed25519)
Created directory '/c/Users/lecht/.ssh'.
Your identification has been saved in /c/Users/lecht/.ssh/id_ed25519.
Your public key has been saved in /c/Users/lecht/.ssh/id_ed25519.pub.
The key fingerprint is: SHA256:3SD7FD5KyDaalghLNEINP8/uEiX6l1McTbcMJaBtdq4 <mailys.lechtchev@ulb.be>*

![](../images/clé-ssh.JPG)

Une fois la clé générée, je peux la copier pour l'ajouter aux préférences de mon gitlab personnel. La commande **cat ~/.ssh/id_ed25519.pub | clip** me permet de le faire. Maintenant que je peux jongler avec les deux, je vérifie si la connexion est faisable ou non à l'aide de la commande **ssh -T git@gitlab.com**. J'obtiens la réponse : *The authenticity of host 'gitlab.com (172.65.251.78)' can't be established.
ED25519 key fingerprint is SHA256:eUXGGm1YGsMAS7vkcx6JOJdOGHPem5gQp4taiCfCLB8.
This key is not known by any other names.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added 'gitlab.com' (ED25519) to the list of known hosts.
Welcome to GitLab, @mailys.lechtchev!* Parfait, ça a fonctionné!

La prochaine étape est de cloner avec SSH sur la page de destination de mon projet. Je tape : **git clone git@gitlab.com:fablab-ulb/enseignements/2021-2022/fabzero-design/students/mailys.lechtchev.git** et j'obtiens ceci.

![](../images/clone-git.png)

Ensuite je tape **cd** et **ls** pour les bonnes vérifications d'emplacement de fichiers.

![](../images/cd-ls.png)

## Documentation de notre site internet

L'exercice demandé était de remplir l'*index.md* qui parle de nous, notre vie et notre choix d'option. Il fallait également documenter notre formation git. Pour cela, deux façons pour le faire ; soit télécharger [Atom](https://atom.io/), soit le faire directement depuis le GitLab via la fonction **edit**.

- Sur GitLab, l'enregistrement est simple, il suffit de cliquer sur **Commit changes** puis de taper la commande **git -pull** pour synchroniser les données vers le terminal.

- Sur Atom, il faut aller dans *file*, *add project folder* et sélectionner le bon dossier dans notre ordinateur. Une fois ouvert, la modification est simple d'utilisation, tout comme GitLab. Pour la synchronisation des données, il faut cliquer sur ces éléments.

![](../images/atom-push.png)

Dans le terminal, les commandes à effectuer sont **git add -A**, **git commit -m "*modifications à nommer*" ainsi que **git push**. Ces commandes vont servir à enregistrer les modifications apportées et à les envoyer vers le projet d'origine sur le GitLab. D'autres commandes sont réalisables comme *fetch*, par exemple, qui sert à vérifier si la version est la dernière ou non. Il y a aussi la commande *git status* qui peut être utile afin de vérifier le statut actuel. Voici un exemple de certaines commandes réalisées par mes soins.

![](../images/all.png)

En ce qui concerne les images, elles se doivent être les plus comprimées possibles afin de ne pas surcharger le site. Cela peut se faire sur un programme, sur un site ou bien directement via le terminal. Elles se chargent directement sur le GitLab dans le dossier *images*.

Ne bien sûr pas oublier une étape importante, celle de modifier les données de notre site. Appuyer sur **edit** puis **commit changes** et laisser un commentaire afin de vérifier par la suite ce qui a été modifié. On peut également vérifier que ce soit ici ou ailleurs l'aperçu de notre site en cliquant sur *preview* (à droite de *write* quand on est dans le mode *edit*). Je trouve cela très utile surtout pour vérifier si tous les liens et toutes les images sont bien passées!

![](../images/site.png)

## Vidéos et sites utiles à la compréhension de certaines fonctionnalités

[Compresser une image](https://www.youtube.com/watch?v=WaKMw7oKIfw&ab_channel=SagarS)

[Aide mémoire](https://www.markdownguide.org/cheat-sheet/)
