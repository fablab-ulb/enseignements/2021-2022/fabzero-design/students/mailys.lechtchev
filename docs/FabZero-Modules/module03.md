# 3. Impression 3D

Cette semaine, j'ai appris à imprimer un objet 3D. C'est ce que même objet que j'ai modélisé sur Fusion 360 lors du module 2.

La bonne nouvelle est qu'il ne faut pas modéliser l'objet plusieurs fois, il faut simplement l'exporter depuis Fusion 360 en fichier *stl* afin de pouvoir l'importer dans le programme [PrusaSlicer](https://www.prusa3d.com/prusaslicer/) que nous avons dû télécharger pour créer un G-code nécessaire à l'impression. Lors du téléchargement, il est demandé de choisir un modèle d’imprimante (MK3 ou MK3S pour ma part) ; ce n’est pas grave si l’on s’est trompé, cela peut toujours être modifié par la suite dans les réglages situés dans la barre d’outils à droite du plateau d’impression. Ce programme sert à la modification de paramètres tels que le choix du matériau utilisé, le remplissage de l'objet, le support, etc. Il est utile de se mettre en mode expert afin d'accéder au maximum de paramètres. Il est très important d'être minutieux, de bien réfléchir et de vérifier à deux fois afin d'éviter tout désagrément. Il est facile pour l'imprimante de réaliser une erreur car un paramètre mal configuré peut entraver le bon déroulement de l’impression.

Pour ma part, je n'ai pas eu à changer énormément de paramètres, dû à la forme assez simple de mon objet, un porte-parapluie. Une fois importé dans le programme, l'objet apparaît sur le plateau d'impression de 15x15 cm. Il faut le mettre à bonne échelle car l'imprimante n'ira pas au-delà du plateau. Pour ma part, j'ai réduit l'objet à 10% ; la fonction se trouvant en bas à droite une fois qu'on a cliqué sur l'objet.

![](../images/taille.png)

Une fois ce réglage fait, il faut choisir son matériau (en fonction de la disponibilité du FabLab). Dans la version que j'ai téléchargée, le prusa pla n'existait pas, j'ai donc dû aller le chercher dans la bibliothèque via cette touche.

![](../images/filament.png)

Une fois le matériau choisit, il faut entrer les données de l'imprimante à disposition. Pour ma part, ce sont ces données que j'ai encodé.

![](../images/modifs-couches.png)

Ensuite, il fallait réfléchir au remplissage. Étant donné que le pourtour de mon objet est assez fin, je n'en ai pas besoin. On peut également ajouter des bordures, qui peuvent aider à ne pas que l’imprimante se décolle de l’objet mais je n’en ai pas besoin non plus. Par contre, ce qui peut être utile, ce sont des supports. La zone sensible du porte-parapluie se trouve là où il n'y a plus de matière. J'ai donc jonglé avec plusieurs paramètres pour trouver le bon compromis entre temps d'impression et matière imprimée.

![](../images/supports.png)

Tous ces changements n'ont pas eu un grand effet, en appuyant sur *découper maintenant*, j'arrivais toujours à 2h d'impression pour un objet de 6 cm de haut. Après discussion avec une membre du FabLab, on s'est dit qu'il fallait essayer en retournant l'objet. Malheureusement cela ne peut pas fonctionner car l'objet a un socle et sans supports, le socle s'affaissera à l'impression. On s'est donc dit que je pouvais tenter une impression "simple". Sans supports, l'objet de 6 centimètres de haut prendra 51 minutes d'impression. La petite astuce est qu'il faudra augmenter la vitesse de la machine à l'endroit critique (on peut déjà voir l’avancée de l’impression via la jauge orange sur la droite). Les modifications sont donc faites, je peux sortir le G-code pour le déposer sur une carte sd.

![](../images/g-code.png)

Une fois devant l'imprimante, on place la carte sd, on choisit le bon fichier et on attend que l'imprimante et le plateau atteignent la bonne température. 215° pour l'imprimante et 60° pour le plateau. Attention de bien nettoyer le plateau à l'acétone avant toute impression ! Et attention également à ne pas ouvrir de fenêtres, cela peut altérer l'impression.

![](../images/calibrage.jpg)

Quand les températures sont atteintes, l'impression peut être lancée en vitesse 100. Cela se réalise par couche et il est important de rester devant durant les trois premières couches au moins car elles peuvent être critiques, c'est souvent à ce moment-là qu'il peut y avoir des problèmes. Pour ma part, le début s'est bien passé.

![](../images/début.jpg)

Arrivé à l'endroit fragile de mon objet, je commence à augmenter petit à petit la vitesse d'impression à l’aide de la roulette à côté de l’écran. Étant donné que c'était une première pour moi, je n'ai pas trop osé accélérer énormément. La dernière vitesse enregistrée était de 226 (le maximum étant de 300).

![](../images/226-degrés.jpg)

Hormis cette manipulation quelque peu stressante, je n'ai eu qu'à regarder le porte-parapluie grandir. C'était très satisfaisant d'ailleurs !
Finalement, ce que je retiens de cette impression est qu’il est nécessaire de bien configurer les paramètres afin d’éviter des impressions répétitives pour arriver à l’objet final. Ce que j’aurais pu changer pour une meilleure impression ? L’épaisseur du pourtour de l’objet certainement, pour une meilleure impression à l’endroit critique.

![](../images/final.jpg)

## Problèmes rencontrés

Comme vous avez pu le remarquer sur les images, nous avons décidé avec Sarah d'imprimer nos deux objets sur la même plaque d'impression. Nous l'avons fait car nos paramètres choisis étaient les mêmes et le temps d'impression était pratiquement le même également.

Malencontreusement, une fois la cagette de Sarah finie, la buse n'en a fait qu'à sa tête! A hauteur du point maximal de la cagette, l'impression du porte-parapluie s'est faite différemment, il y eu des sortes de lignes diagonales créées. A mes yeux, ce n'est pas dérangeant mais à l'avenir, je sais que cela peut créer des soucis.

![](../images/erreur.jpg)

Souci numéro 2, l'endroit critique de mon objet. Malgré l'augmentation de vitesse à cet endroit, le filament s'affaissait et se dirigeait vers l'intérieur dû à la chaleur et la gravité terrestre.

![](../images/erreur2.jpg)

## Lien vers le fichier Fusion 360

[Porte-parapluie](https://a360.co/3pl4WCT)

[Porte-parapluie stl](../images/porte-parapluie-stl.stl)

## Vidéos

Si on souhaite changer de couleur de bobine, il faut, une fois devant l'écran de commandes, sélectionner *load the filament*. Insérer le fil de couleur voulu et suivre les instructions données. Une fois la couleur nette atteinte, l'impression peut être lancée.

![](../images/changement-filament.mp4)

Deux vidéos supplémentaires (pour le plaisir des yeux) sur la création de la base et sur le moment un peu plus critique de l'impression que j'ai choisie.

![](../images/création-base.mp4) ![](../images/moment-critique.mp4)

## Bonus

Voici quelques exemples des impressions que d'autres étudiants ont réalisées.

![](../images/all.jpg)
