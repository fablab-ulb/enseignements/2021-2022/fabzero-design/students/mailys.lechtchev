# 2. Conception Assistée par Ordinateur : Fusion 360

Cette semaine, un intervenant extérieur est venu nous donner les bases utiles à la modélisation 3D sur le programme Fusion 360.
L’exercice, globalement, était de réaliser un cube arrondi avec des trous. Nous avons appris à travailler avec des congés, des axes, des rotations, des réseaux, des esquisses, des extrusions, etc. Je vais rapidement les lister afin de pouvoir se faire une idée globale du fonctionnement du programme.

- Toute modélisation en trois dimensions demande en premier une esquisse en deux dimensions, peu importe le plan (x, y ou z). Pour cela, il faut le trouver dans la barre d'outil **créer**. C'est avec cet outil que l'on pourra créer des formes. Une fois l'esquisse finie, il faut cliquer sur *terminer l'esquisse* en haut à droite

![](../images/esquisse.jpg)

- Ensuite, nous avons appris à réaliser un cube à partir de la forme créée. C'est plutôt simple, il suffit de séléctionner l'esquisse dessinée pour l'extruder. Cet outil se trouve juste à droite de l'outil précédemment utilisé. Une fois cela compris, libre à nous de choisir une valeur d'extrusion. Les possibilités sont variées pour l'extrusion. Par exemple, elle peut se faire d'un côté, des deux ou symétriquement

![](../images/extrusion.png)

- Autre exemple pour une extrusion, avec une valeur négative, cela va enlever de la matière à l'extrusion créée de base

- Pour donner une forme arrondie à notre cube, on peut utiliser la fonction **congé** que l'on peut trouver dans **modifier**. Petite astuce : le programme permet de revenir en arrière à n'importe quel moment de la modélisation. Il est donc utile de le faire à la fin de la modélisation dans ce cas là afin de ne pas créer de problème avec les congés. Le tout doit être sélectionné pour ensuite cliquer sur congé et cela se fait automatiquement (la valeur est à choisir)

![](../images/congé.jpg)

- Pour répéter l'action sans le refaire sur chaque plan, on peut utiliser la fonction **réseau** -> **réseau circulaire** trouvée dans l'onglet *créer*. Cette fonction sert donc à dupliquer des éléments selon le nombre et les degrés que l'on va entrer. Il est obligatoire de sélectionner un objet et un axe. Il est bien entendu possible de créer soit-même un axe dans l'onglet **construire**. Attention à bien choisir les éléments afin de créer le réseau comme voulu

![](../images/réseau-circulaire.png)

- Si on veut ajouter une esquisse à l'élément existant et que l'on veut qu'ils coïncident ensemble, les contraintes que l'on peut choisir dans la barre d'outils sont variées ; égal, parallèle, perpendiculaire, colinéaire, tangente, symétrique, etc.

![](../images/esquisse2.jpg)

Voici l'objet que nous avons réalisé lors de la formation du programme :

![](../images/cube-étapes.jpg)

## Modélisation d'un objet

Etant dans le module 3 au musée du design, j’ai dû choisir un objet sur place qui me plaisait. Un porte-parapluie de Kartell a attiré mon regard, je l’ai trouvé simple et élégant.

![](../images/Name.png) ![](../images/Photos.png) ![](../images/Description.png)

La première étape est de comprendre comment est construit l'objet. Pour cela, un croquis et des mesures sont les bienvenus et, une fois sortie du musée, je me suis également renseignée sur le site de [Kartell](https://www.kartell.com/US/fr/kartell-loves-the-planet/portaombrelli/07610) afin d'obtenir un maximum d'information sur l'ojet choisit.

![](../images/dessin.jpg) ![](../images/metré.jpg) ![](../images/moyen.jpg)

Passons à la modélisation. Il faut impérativement commencer par une esquisse et choisir un plan de départ. Ce sera le plan horizontal pour cet objet.

![](../images/1cercle.png)

Après avoir dessiné la base ronde, il faut extruder le tout. Deux solutions s'offrent à moi : soit extruder pleinement pour ensuite retirer de la matière, soit extruder finement le pourtour. J'ai choisi la deuxième option.

![](../images/2extruder.png) ![](../images/3extruder.png)

Avec ce deuxième choix, la base n'existe plus. Il faut donc réitérer la première étape et extruder pour ajouter une épaisseur à la base du porte-parapluie.

Maintenant que le cylindre est fait, il faut le trouer. Pour cela, je réalise une esquisse sur le plan horizontal.

![](../images/4rectangle.png)

L'étape compliquée ici pour moi (alors que c'était au final très simple) était de déplacer cette forme à bonne hauteur. J'ai pas mal chipoté pour cela mais j'y suis arrivée.

![](../images/5déplacer.png)

De nouveau, on extrude et la matière du cylindre s'enlève automatiquement.

![](../images/6extruder.png)

La dernière étape est de la finition. J'ai donc arrondi le haut du cylindre ainsi que les coins de la partie supprimée.

![](../images/7arrondir.png) ![](../images/8arrondir.png)

Et voici le résultat!

![](../images/pp-final.png)

## Lien vers les fichiers Fusion 360 et stl

[Porte-parapluie](https://a360.co/3pl4WCT)

[Porte-parapluie stl](../images/porte-parapluie-stl.stl)

## Vidéos utiles à la réalisation de l'objet

[Fusion 360 Comment déplacer un croquis](https://www.youtube.com/watch?v=7AQm1xMipHM&ab_channel=MufasuCAD)

[How to Move a Sketch in Fusion 360!](https://www.youtube.com/watch?v=O4oTu-nhmAE&t=221s&ab_channel=adamjames)
