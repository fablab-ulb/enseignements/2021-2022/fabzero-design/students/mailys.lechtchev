# 5. Usinage assisté par ordinateur : Shaper Origin

## L'utilité de la machine?

C'est une découpeuse CNC portable qui se base sur un système de vision par ordinateur. Cela est pratique dans le sens où il y a un positionnement en temps réel sur la surface de travail. L'autre particularité est que la machine fonctionne toujours avec un aspirateur relié.

![](../images/shaper.jpg)
*Photo issue de Shop3D.ca*

![](../images/aspirateur.jpg)
*Photo de Camille*

## Apprentissage et essais ultérieurs

Tout comme les découpeuses laser que nous avons étudiées en semaine 4, le dessin à découper doit être réalisé sur un programme de vecteur, comme Inkscape. Il faut le sauvegarder en svg pour être lisible par la machine.

Concernant les caractéristiques et les précautions d'usage, je vous renvoie vers le [guide d'utilisation du Fablab](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/dc6cd87ffe1610ae9d5b1dd8bab55d222f78ae49/Shaper.md).

**Etapes à suivre**

Les étapes de configuration et de planche peuvent évidemment s'inverser, cela n'a pas une grande importance.

- Coller le double-face sur une planche "brouillon" afin de stabiliser la planche sur laquelle on découpe. On peut également pour les planches épaisses, par précaution, utiliser des serre-joints.

![](../images/double-face.jpg)
*Photo de Camille*

![](../images/serre-joints.jpg)

- Coller les capteurs qui serviront à délimiter l'espace de découpe (maximum 8cm d'espacement et minimum 4 dominos par bande). Attention à ne jamais sortir de l'angle au risque de stopper la découpe (si la machine ne les voit plus, elle ne peut plus fonctionner).

![](../images/dominos.jpg)

- Scanner les dominos placés. Une fois qu'ils deviennent bleu sur l'écran, c'est bon

![](../images/scan.png)
*Photo issue de Shaper*

- Soit dessiner directement sur l'écran soit importer un fichier (dans notre cas : import via une clé usb). Quand le fichier apparait ; on peut visualiser son emplacement, son échelle et changer en fonction des besoins. Si on est satisfait, cliquer sur **placer**.

![](../images/import.png)
*Photo issue de Shaper*

Si on souhaite changer la fraise, il faut déverrouiller la broche (côté gauche) et dévisser la vis (côté droit). Les opérations inverses sont à réaliser pour commencer une découpe

![](../images/broche.jpg)

- Allumer la machine et l'aspirateur en appuyant sur leur intérrupteur ON/OFF respectif

- Commencer la découpe en appuyant sur **fraiser**

- Se déplacer en suivant la ligne choisie (quand elle devient verte, appuyer sur le **bouton vert** sur la poignée droite pour lancer la découpe)

![](../images/ligne-verte.jpg)

![](../images/vidéo.mp4)

Pour stopper la découpe, appuyer sur le **bouton orange** sur la poignée gauche. Il est impératif de le faire à chaque tracé différent (à un angle par exemple)

On peut voir sur l'écran ici les possibilités de découpe.

![](../images/écran.jpg)

- **profondeur** de découpe
- **endroit** : choix de la découpe ; sur la ligne, à l'extérieur, à l'intérieur,... Dans certains cas, ce paramètre ne veut pas se changer. J'ai eu le cas lors d'un premier essai avec mon groupe du musée et malheureusement on ne sait toujours pas à quoi c'est dû. On a supposé que cela venait de l'épaisseur de trait choisi
- **décalage** de la fraise
- **diamètre** de la fraise
- **touché vertical** : pour vérifier à quelle distance est la planche
- **vitesse** : le mode automatiquement peut être choisi. Pour la plupart des matériaux, la vitesse est de 250

## Résultat du test

A droite, une découpe avec un paramétrage de découpe peu profond par rapport au matériau. A gauche, le deuxième essai avec un paramétrage plus plausible.

![](../images/coupe-shaper.jpg)

## Liens utiles

[Shop3d.ca](https://shop3d.ca/products/shaper-origin-the-handheld-cnc-router)

[Shapertools](https://www.shapertools.com/fr-fr/origin/overview)
