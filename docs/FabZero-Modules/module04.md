# 4. Découpe assistée par ordinateur

## Apprentissage des machines

Cette semaine, à l'aide d'un membre du fablab, Alex, nous avons appris à utiliser trois machines. La Lasesaur, l'Epilog Fusion Pro 32 et la Cameo.

![](../images/machines.jpg)

La Lasesaur et l'Epilog sont toutes deux des machines à découpe laser et leurs fonctionnalités sont similaires. Leurs différences se portent sur leur programme de découpe, leur taille ainsi que les matériaux à utiliser ou non. Les voici ainsi qu'une liste des précautions à prendre.

![](../images/matériaux.png) ![](../images/précautions.png)

Pour les trois machines, le dessin créé doit être sous forme de vecteurs. Cela peut être réalisé sur le programme [Inkscape](https://inkscape.org/fr/) facilement téléchargeable. Pour la machine Cameo, le logiciel [Silhouette Studio](https://www.silhouettefr.fr/applications-logiciels/) fait l'affaire.

Comment fonctionne une découpeuse laser ? C’est une technique de découpe et de gravure fonctionnant à base d’un laser puissant et très précis qui se concentre sur une petite surface du matériau utilisé. En règle générale, cette machine fonctionne avec un ordinateur, qui lui va dicter le chemin à suivre.
Plus la buse du laser est lente et plus puissante, plus le matériau sera découpé. A l’inverse, plus elle est rapide et moins puissante, plus le matériau sera gravé.

- Lasersaur

L'allumage de cette machine est le plus complexe des trois car il faut penser à la sécurité avant tout. Il faut penser à allumer le refroidisseur à eau, l'extracteur de fumée et ouvrir la vanne d'air comprimé.

![](../images/lasercutter.png)

Dans le cadre de l'exercice de cette semaine, je n'ai pas utilisé cette machine sauf pour réaliser quelques tests de découpe. Nous avons commencé par groupe la réalisation d'un tableau de différentes valeurs de vitesse et d'intensité afin d'observer la réaction du matériau en fonction des paramètres. Nous avions décidé de le réaliser sur Affinity Designer, malencontreusement le fichier n'étant pas pris en compte par le programme de la découpeuse laser, il a fallu recommencer. C'est assez simple, pour chaque valeur donnée, il faut une couleur donnée et ces valeurs se changent sur la droite. Il faut être méthodique quand on le fait car il est facile de se tromper. Attention à bien organiser les couleurs de sorte à réaliser les formes intérieures en premier sinon le matériau risque de bouger (pour cela également que l'on voit sur la première photo qu'il y a des poids sur le matériau).

Comme on peut le voir en comparant le notre et celui du Fablab, notre test n'a pas été le meilleur.

![](../images/laser.jpg)

Pour configurer la buse correctement sur le matériau dans la machine, il faut utiliser la fonction **move** et se placer au bon endroit. Une foisque cela est fait, il faut vérifier si c'est le bon emplacement. Pour cela, il faut cliquer sur les **deux flèches** à côté de *run*, ce qui va obliger la buse à faire le tour du matériau. Quand tout est en ordre, il faut appuyer sur **run** pour lancer la découpe.

Pour les deux machines suivantes, je vais expliquer leur fonctionnement brièvement étant donné que je développe bien plus le sujet plus bas.

- Epilog

Premièrement il faut tourner la clé afin d'activer la découpeuse laser. Pour la découpe sur cette machine, il faut allumer le refroidisseur. Attention à bien l'éteindre entre chaque découpe, dès que la machine ne tourne pas. Ensuite il faut se placer devant la machine et sélectionner le bon fichier dans le menu déroulant. Une fois choisit, on appuie sur play et la machine se met en route.

![](../images/epiloglaser.jpg) ![](../images/menu.jpg)

- Cameo

Une fois la machine allumée, il faut **charger** la feuille ou le rouleau sur lequel on souhaite découper et dessiner. La feuille choisie doit se déposer sur un tapis de découpe collant prévu pour la machine. Quand celle-ci est bien rentrée, il faut déplacer la buse au bon endroit à l'aide de flèches sur l'écran.

![](../images/charger.jpg) ![](../images/déplacer.jpg)

Deux buses sont disponibles pour cette machine : la découpe en rouge et le dessin en vert.

![](../images/découpe-cameo.jpg)

## Inspiration, exploration et prototype

Cette semaine nous avons eu pour exercice la réalisation d’une lampe pour enfants en plaque de polypropylène. Nous avions plusieurs choses à respecter : le fait de ne pas utiliser de colle ou de fixation ainsi que la condition de l'économie de matière.

1. La première idée qui m'est venue a été fort bloquée par la forme de la lampe de bureau choisie. Je n'avais que celle-ci à disponibilité sur le moment et je n'ai pas forcément réfléchi à un fil conducteur. Les formes sont venues simplement et l'idée est venue par la suite. Le but de ces parties qui s'emboitent était qu'elle puisse s'écarter ou se rapprocher facilement pour avoir une libre utilisation de la lumière et des ombres. Le problème flagrant était que, nous le savons tous, l'enfant est touche-à-tout et l'objet aurait pu être endommagé rapidement. J'ai finalement abandonné l'idée.

![](../images/prototype1b.jpg)

Le seul point que j'ai gardé de ce test est le système d'accroche ; des encoches et des parties qui s'emboitent dans ces dernières.

2. Pour ma seconde idée donc, j'ai conclu qu'il fallait réellement se mettre à la place de l'enfant et de ce qu'il souhaiterait comme lampe. J'ai cherché dans mes souvenirs et j'ai trouvé les lampes que j'avais étant plus jeune. Je me souviens encore de ces jeux de lumière qui se créaient grâce à ces motifs encrés dans la partie recouvrante. J'ai donc réfléchi de cette manière.

![](../images/inspi.jpg)

Pour la forme de cette lampe, j'ai voulu qu'elle soit allongée, qu'elle s'étire réellement vers le plafond.

![](../images/prototype2.mp4)

3. L'idée finale est survenue suite à une entrevue avec Hélène, Gwen et Victor. L'idée globale des motifs pour enfants n'était pas mauvaise, cela bloquait plus au niveau de la source lumineuse choisie. Mais l'intérêt dans cette dernière est qu'elle peut se pencher. Nous avons donc décider de la conserver mais qu'il fallait donc que je me concentre en premier sur le moyen d'accroche entre la source et la lampe créée. Après discussion avec Sarah, nous sommes finalement arrivées à une forme qui pouvait tenir la route. Et après observation, cela me faisait penser à la lampe Pixar.

![](../images/prototype3.jpg) ![](../images/prototype3b.mp4) ![](../images/prototype3c.jpg)

Finalement, il faut mesurer les bonnes dimensions de la source de lumière afin que la lampe imaginée corresponde parfaitement. Pour cela, j'ai pris un mètre de couture et j'ai exagéré les mesures afin de ne pas être trop collé à la source de lumière. L’importance dans le patron de cône que j’ai réalisé se trouve dans le système d’accroche. Il me fallait être précise pour définir les bons endroits de découpe et de double épaisseur.

![](../images/mesures.jpg)

## Réalisation vectorielle

Pour la partie dessinée à l'ordi, j'ai décidé de dessiner cela sur un programme que je connais pour ensuite le transférer en *svg* sur le programme Inkscape (dxf ou dba fonctionnent aussi). J'ai choisi Autocad étant donné que c'est un programme que j'utilise souvent. Il faut faire attention à ne pas créer de polylignes car la découpeuse laser fonctionne avec des vecteurs. Il faut également être attentif aux différentes couleurs qu’on utilise. Les contrastes doivent être relativement présents afin de faciliter la tâche à la découpeuse laser. Pour ma part, je n’ai choisi que deux couleurs, une pour la découpe, l’autre pour la gravure. Dans Autocad, cela se réalise sous forme de calques que l’on peut créer dans les propriétés du calque.

![](../images/calques.png)

Pour la réalisation de la lampe, j’utilise les outils **cercle**, **ligne** et **ajuster**. En même temps que les tracés se réalise, j'écris sur mon clavier la valeur en centimètres que je souhaite et le tour est joué.

![](../images/outils.png)

- Je commence par les formes principales qui sont les deux cercles intérieurs et extérieurs
- Je dessine les lignes qui définissent les parties qui se rejoignent entre elles. Pour ces lignes, j'ai choisi un angle à l'œil qui se rapprochait le plus possible de mon prototype
- Je place des traits de constructions pour m’aider à comprendre le fonctionnement et la torsion de l’objet créé. Ainsi, je peux placer mon système d’accroche
- J’utilise l’outil **ajuster** pour supprimer tous les traits inutiles
- Finalement, je dessine les motifs imaginés pour les enfants. Je dessine tout d’abord la forme intérieure en polyligne, je la sélectionne et je tape la fonction **décaler** pour obtenir l’autre forme que je place dans le calque rouge. Ensuite, je sélectionne les éléments et je tape la fonction décomposer afin d’avoir uniquement des lignes seules.

![](../images/lampe-étapes.png)

## Découpe

Une fois le programme Inkscape démarré, j’ouvre mon document en svg. Une fois ouvert, il faut vérifier si la mise à l’échelle et les paramétrages d’unités sont corrects. Pour cela, il faut se rendre dans les propriétés du document et choisir les unités dont on a besoin.

![](../images/unités.png)

Ensuite, il faut aller dans **fichier** - **imprimer** pour ouvrir le document dans le programme Epilog, lié à la découpeuse laser. C’est dans ce programme que l’on pourra modifier les paramètres des opérations à réaliser. J’ai choisi les vitesses et puissances en fonction des exemples déjà disponibles au Fablab. La fréquence, quant à elle, restera toujours à 100%.

![](../images/exemple-découpe.jpg) ![](../images/opérations.jpg)

On peut remarquer qu’avec ces paramètres et cette taille peu imposante, la découpe est très rapide ; 3 minutes et 34 secondes.

![](../images/lampe-découpe.mp4)

Les prototypes réalisés pouvaient réellement fonctionner mais une fois en polypropylène et avec les motifs imaginés, le rendu n’était clairement pas celui imaginé. Je ne suis pas convaincue de cette lampe finale et je m’attendais aux remarques reçues lors de la correction. Je ne vais pas dire que j’aurais pu obtenir la lampe parfaite, mais avec moins de stress, plus de temps et plus d’accessibilité aux machines, cela aurait pu mieux se dérouler. Néanmoins, je suis plutôt satisfaite de la partie en double couche avec le nuage car à mes yeux, c’est la partie la plus réussie et cela donne bien. Si c’était à refaire, je mettrais plusieurs couches et je viendrais probablement placer une partie supérieure afin d’avoir des jeux d’ombres, de lumière et de formes à l’aide de la réflexion.

![](../images/lampe-finale.jpg)

Concernant la découpe, je ne suis pas satisfaite car je pense avoir mal programmé les paramètres de vitesse et d’intensité. Je me suis retrouvée avec des brulures et le rendu n’est pas fameux. Mais je pars du principe que c’est en commettant des erreurs que l’on apprend ! Il aurait peut-être été intelligent que je réduise les deux paramètres et de passer deux fois sur le tracé afin d’avoir un rendu plus propre.

## Autocollant vinyle

J'ai décidé de partir d'un dessin réalisé à la main. La première étape est de *nettoyer* ce dessin. Pour cela, je suis allée sur Photoshop. Il faut ouvrir le dessin pour ensuite réaliser la première étape qui consiste en la mise en noir et blanc du dessin. Cela supprime toutes informations chromatiques.

![](../images/niveau-de-gris.png)

Une fois cette opération réalisée, il faut faire *ctrl+M* (sur Windows) afin d’ouvrir la barre d’outils qui va nous servir à clarifier les contrastes. Ces deux pipettes servent à échantillonner dans l’image pour définir le point noir et blanc. L’astuce est de choisir sur le dessin les points les plus foncés et les plus clairs afin de faciliter la mise en vecteur par la suite.

![](../images/pipettes.png)

Voici le avant/après.

![](../images/001a.jpg) ![](../images/001b.jpg)

Pour la découpe au vinyle, il faut donc pouvoir le vectoriser. Avec l’aide de Gwen, nous avons glissé l’image en jpeg sur le programme Silhouette Studio. L’opération est très simple : il faut cliquer sur le logo qui ressemble à un papillon. Ensuite il faut jouer avec le **seuil** afin d’obtenir un résultat qui nous convienne et appuyer sur vectoriser.

Cette opération est également disponible dans le programme Inkscape sous **chemin** - **vectoriser un objet matriciel**.

![](../images/vectoriser.jpg)

Maintenant, je trace un cercle autour de mon dessin car cela sera la partie découpée, contrairement au reste qui sera dessiné. Il y a donc deux couleurs à choisir pour les différentes fonctions de la machine.

![](../images/cercle.jpg)

Il faut bien faire attention à mettre les bons paramètres aux bonnes couleurs car on peut vite se tromper ! La force va vraiment dépendre du matériau choisit. Pour ma part, il y a beaucoup de dessin, j’ai donc choisi quelque chose de rugueux et de relativement épais.
-	Croquis : force 18, vitesse 10
-	Découpe : force 10, vitesse 5

![](../images/croquis.jpg)

Quand les réglages sont finis, il faut cliquer sur envoyer.

![](../images/vidéo-croquis.mp4)

Pour la découpe, la machine va faire quelques tests au préalable afin de vérifier si tout est bon.

![](../images/vidéo-découpe.mp4)

![](../images/autocollant.jpg)

Une fois le travail terminé, il faut **décharger** afin de sortir la feuille de la machine et de remettre tout à l’état initial.

## Vidéos utiles à la réalisation de l'objet

[Transformer un dessin scanné en objet vectoriel](https://www.youtube.com/watch?v=ucXk0uakISY&ab_channel=DidierMazier)

## Liens vers les fichiers

[Guide Lasersaur](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/Lasersaur.md)

[Guide Epilog](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/EpilogFusion.md)

[Prototype 2 en svg](../images/lampe1.svg)

[Prototype 3 en dwg](../images/lampe2.dwg)

[Prototype 3 en svg](../images/lampe2.svg)

[Autocollant en svg](../images/autocollant.svg)
